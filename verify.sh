
echo "Starting verification script"
echo "Trying to execute ./helloworld"

OUTPUT=`./helloworld`
RETVAL=$?

if [ $RETVAL -eq 0 ]; then
  echo "Retval = 0"
else
  echo "Retval != 0"
  exit 1
fi

if [ "$OUTPUT" == "Hello World!" ]; then
  echo "Output correct"
else
  echo "Output not correct"
  exit 1
fi

echo "finished executing test script"